## Mini Exercise Two - Geometric Emojis ##

**URL's**

[Link to RUNME of Extinction Rebellion Logo](https://janecl.gitlab.io/ap-2020/MiniEx2/)

[Link to RUNME of sweating globe emoji](https://janecl.gitlab.io/ap-2020/MiniEx2.0/)

Link to repository of Extintinction Rebellion emoji: https://gitlab.com/JaneCl/ap-2020/-/blob/master/public/MiniEx2/sketch.js

Link to repository of sweating globe emoji: https://gitlab.com/JaneCl/ap-2020/-/blob/master/public/MiniEx2.0/sketch.js

### My Programs and whar I learnt ###

For both emojis I wanted to focus on the climate crisis as I felt a lack of representation in the emojis I could find online. 
For my first emoji I wanted to recreate [The Extinction Rebellion logo](https://rebellion.earth/), which is a global environmental movement who uses nonviolent civil disobedience to 
make the governments act on the climate crisis among other things. 
Because the logo itself is pretty simple, I wanted to challenge myself and make it rotate in 3D. To do this I added WEBGL as an argument in createCanvas to create the third 
dimenseion and then I chose to use an ellipsoid as my emoji 'head'. Before starting I thourght that was going to be the hardest part although it turned out to be the 
easier part of this project. 
I learnt that when adding a third dimension to a canvas you center the x- and y-axis of your coordinates which means there are now also negative values - this I had to realise 
before any of my placements made sense.
When it came to adding on the logo itself, I started out using vertex(); to create the shape inside the circle. It worked pretty well when I eventually figured out the 
right coordinates, and I realised that to get the shape to lift of, of the center axis, I just had to add an extra dimension [Z] to my function.
To make the circle around the X turned out to be a lot harder as I couldn't just add an extra argument to make it lift of of the center axis. 
Instead I learned that I had to use the functions push(), pop() and translate(0,0,51) to make it happen. These functions are great to make a 
part of your code move on the canvas. In theory it worked great but it didn't look very smooth as I found out that 2D shapes have a hard time functioning in a 3D world
espectially if you want the stroke(); to be pretty thick. Below is a screenshot of how that looked and the functions to make it is still in the program although hided as 
comments if you want to see how it was done. 

![Faulty circle](screenshot_faulty_circle.png)

To make the circle look smooth, I had to make the circles with 3D objects. As you cannot make a hollow cylinder, what you see is to cylinders in the same spot, where the 
smallest one (the green circle) sticks out from the big circle (black). Noah was a big help in figuring out how to do this and how to make the cylinders lie down and 
rotate in the same speed as the circle. The X inside the circle is still 2D, and you can see it moving around if you go to the RUNME, with more time I would probably have
figured out a way to use plane() or some other 3D shape to create the inside of the logo as well. 

![Screenshot of both emojis](screenshot_both_emojis.png)

For my second emoji I imagened the red headed sweating emoji with its tongue out and I wanted to make it the earth that was sweating. 
To do so I first preloaded a picture of the earth and added it on with the coordinates and size: image(img,275,150,250,250); then I made a transparent ellipse to put on 
top to create the redness (I did the same with the cheeks). For the tongue and eyebrows I used curveVertex(); for the first time which I found a bit confusing as some of
the coordinates needs to be repeated to make the shape right which I did not understand. For the drop of sweat (or tear however you want to interpret it) i made an arc() to 
make a half circle and the used vertec(); to create and open triangle on top. The challenge in this project were the many 'homemade' shapes I had to do which I find 
quite time consuming. 

### Interpretation and context ###

Emojis which were once these yellow blobs or heads to make a text a little more exciting and express emotion have become a part of mainstream media and is often seen as a 
reflection of how we see the world as well as a tool for showing your emotions. In the last five years emojis have expanded and developed a lot and as they became more realistic
looking the demand for them to represent the world as it is became higher. In _Modifying the Universal_, Abbing, Pierrot and Snelting focuses on the rising awareness of 
identity politics and the challanges Unicode and big tech-companies have had in regards to this. From my point of view there are a huge gap in the emoji world concerning
the climate crisis. As the biggest challenge this developed world has ever seen it seems wild to me that one of the few if not only emoji-symbols reflecting the climate 
crisis is the recycling symbol. In light of the earth literally being on fire several places around the world I wanted to make a sweating globe - an alternative idea 
was a globe on fire - especially the australian fires has recently been covered heavily in media. My other emoji - the Extinction Rebellion - is quite political as they are a 
nonviolent group that makes 'inconvenient' demonstrations to call out the government to do something about the climate system, biodiversity loss and the risk of social and
ecological collapse. In reality it might be an emoji that could never be made because of it being an activist group and tech-companies being among their victims. 

Climate change cannot happen through emojis. But in todays society where it has become a big sensation each time new emojis arrive I think the presens and awareness 
climate inspired emojis could generate might help. Also to show that these big tech-companies take the climate serious as well which they should as their data-centers are huge
energy offenders.

I believe there will always be an emoji missing. No matter how much they try and represent everybody and everything there will allways be something to do better. The more
detailed and the more people they represent the higher is the demands going to be on the details of an emoji. It has to be more and more specific to _everybody_ which will
be impossible to ever achieve with the way emojis are set up today. 