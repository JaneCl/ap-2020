//The Extinction Rebellion Logo as a rotating emoji
//Ellipsoid gotten from p5*js reference page

let detailX;
function setup() {
  createCanvas(windowWidth, windowHeight, WEBGL);

  /* createSlider is not supposed to be used, although I could not
  figure out how to make the ellipsoid without it
  */
  detailX = createSlider(2, 24, 12);
  detailX.position(10, height + 10);
  detailX.style('width', '80px');
}

function draw() {
  background(180);
  rotateY(millis() / 50);

  //the green 3D circle
  fill(41, 163, 63);
  noStroke();
  ellipsoid(300, 300, 50, detailX.value(), 30);


//the extinction rebillion logo

  //inside the circle (the cross) on one side
  beginShape(LINES);
  stroke(0)
  fill(0,0,255);
  strokeWeight(30);
  vertex(-130, -180,51);
  vertex(130, 180,51);
  vertex(-130, 180,51);
  vertex(130, -180,51);
  vertex(130,180,51);
  vertex(-130,180,51);
  vertex(-130,-180,51);
  vertex(130,-180,51);
  endShape();

  //the cross inside the circle on the other side
  beginShape(LINES);
  stroke(0)
  fill(0,0,255);
  strokeWeight(30);
  vertex(-130, -180,-51);
  vertex(130, 180,-51);
  vertex(-130, 180,-51);
  vertex(130, -180,-51);
  vertex(130,180,-51);
  vertex(-130,180,-51);
  vertex(-130,-180,-51);
  vertex(130,-180,-51);
  endShape();

/*
  //the black circle (faulty)
    //push & pop is used to make the translation only apply to one ellipse
  push();
  translate(0,0,51);
  noFill();
  ellipse(0,0,450,450,30);
  smooth();
  pop();

  //the other black circle (faulty)
    //same practice on the other side of the circle
  push();
  translate(0,0,-51);
  noFill();
  ellipse(0,0,450);
  pop();
*/

//the two cylinders creating the black circle
noStroke();
fill(0);
angleMode(DEGREES);
rotateX(90);
cylinder(240,90);

noStroke();
fill(41, 163, 63);
angleMode(DEGREES);
cylinder(215,102);
}
