//MiniEx 6 - Games with Objects
//Made by Jane Clausen
//Inspiration from Winnie Soon - tofu game

let mode;              //determine whether the game has started
let hearts = [];       //array of hearts
let min_hearts = 6;    //minimum of hearts on the screen at all times
let personSize = {     //the size of the pictures
  w:80,
  h:85
};
let bluePosX;            //position of man
let purplePosX;          //position of woman
let blueScore = 0;       //Start score
let purpleScore = 0;     //Start score
let timerValue = 60;     //timer er 60 seconds


function preload() {
  purple = loadImage('addons/woman.png');
  blue = loadImage('addons/man.png');
}

function setup() {
  mode = 0;       //the game has not started yet
  createCanvas(800,600);
  fill(255);
  textSize(21);
  textAlign(CENTER);

  //startposition of figures
  bluePosX = width/2-50;
  purplePosX = width/2+50;

  for (var i = 0; i < min_hearts; i++) {
      hearts[i] = new Heart();
  }
}


function draw() {
  background(245, 193, 217);

  //Start screen (before pressing enter)
  if (mode == 0) {
    textAlign(LEFT);
    text("You've got ONE minute to collect the most hearts",width/4,200);
    text("Blue player: arrows", width/4,225);
    text("Purple player: A and D", width/4,250);
    textAlign(CENTER);
    text('Press ENTER to start collecting love',width/2,350);
  }

  //when game is on
  if (mode == 1) {

    showHearts();
    image(blue, bluePosX, 415, personSize.w,personSize.h);
    image(purple, purplePosX,415,personSize.w,personSize.h);
    catchingHearts();
    checkHearts();

    //scoreboard at the bottom
    push();
    strokeWeight(3);
    stroke(247, 190, 217,100);
    fill(255, 204, 228);
    rect(10,500,width-20,95);
    pop();
    displayScore();


    //Timer
    if (timerValue >= 60) {
      text('1:00',width/2,555);
    }
    if (timerValue <= 59) {
      text('0:'+timerValue,width/2,555);
    }
    if (timerValue < 10) {
      text('   ' + timerValue,width/2,555);
    }

    //TIME IS UP - see who won
    if (timerValue == 0) {
      fill(245, 193, 217,200);
      noStroke();
      rect(0,0,width,height);
      stroke(235, 52, 76);
      fill(235, 52, 76,150);
      textAlign(CENTER);
      textSize(50);
      text('game over',width/2,height/2);
      noLoop();
      if (blueScore > purpleScore) {
        text('CONGRATS BLUE WON!', width/2,height/2+70);
      }
      if (blueScore < purpleScore) {
        text('CONGRATS PURPLE WON!', width/2,height/2+70);
      }
    }
  }

  //moving blue figure
  if (keyIsDown(65)) {
    bluePosX -=5;
  }
  if (keyIsDown(68)) {
    bluePosX += 5;
  }

  //moving purple figure
  if (keyIsDown(LEFT_ARROW)) {
    purplePosX -= 5;
  }
  if (keyIsDown(RIGHT_ARROW)) {
    purplePosX += 5;
  }
}

//showHearts makes the hearts keep coming
function showHearts() {
  for (let i = 0; i < hearts.length; i++) {
    hearts[i].show();
    hearts[i].move();
    }
}

//the distance that determine whether you have caught the heart or not
function catchingHearts() {
  for (let i = 0; i < hearts.length; i++) {
    let d = (dist(hearts[i].x,hearts[i].y-60,purplePosX+50,430))
      if (d < 30) {
        hearts.splice(i,1);
        purpleScore++;
      }
    }
  for (let i = 0; i < hearts.length; i++) {
    let di = (dist(hearts[i].x,hearts[i].y-60,bluePosX+50,430))
      if (di < 30)  {
        hearts.splice(i,1);
        blueScore++;
      }
    }
}

//Display score of each player
function displayScore() {
  textSize(20);
  textAlign(LEFT);
  text('Blue Player', 20, 545);
  text('You have caught ' + blueScore + " heart(s)", 20, 570);    //blue score
  textAlign(RIGHT);
  text('Purple Player', 780, 545);
  text('You have caught ' + purpleScore + " heart(s)", 780, 570);  //purple score
}

//checks if there are enought hearts on the screen
function checkHearts() {
  if (hearts.length  < min_hearts) {
    hearts.push(new Heart());
  }
}

//starts the game and the timer
function keyPressed() {
  //to start game
  if (keyCode === ENTER) {
    mode = 1;
    setInterval(timeIt, 1000);
  }
}

//timer
function timeIt() {
  if (timerValue > 0) {
    timerValue--;
  }
}
