
class Heart {
  constructor() {
    this.x = random(0,width);
    this.y = random(-height,0);
    this.speed = random(1,5);

  }

  move() {
    this.y += this.speed;
    if (this.y > height) {
      this.y = 0;
      this.x = random(0,width);
    }
  }

  show() {
    stroke(235, 52, 76);
    strokeWeight(1);
    fill(235, 52, 76,100);
    push()
    translate(this.x-100,this.y-120);
    scale(0.3);
    beginShape();
     vertex(300,200);
      bezierVertex(330,40,590,180,300,350);
      bezierVertex(50,200,250,60,300,200);
    endShape(CLOSE);
    pop()

  }
}
