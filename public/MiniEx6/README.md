Link to MiniEx4 - [Games with Objects](https://janecl.gitlab.io/ap-2020/MiniEx6/)

Link to repository: https://gitlab.com/JaneCl/ap-2020/-/tree/master/public/MiniEx6


**Describe work:**

My game is quite simple. Catch the hearts! I made it multiplayer or if you are great at multitasking you can do it by yourself. 
The two players are the emoji man and woman waving and whoever gets the most love in a minute wins. Before the game starts you get short instructions as to which 
keys to press and to start the game you just press ENTER. The scores are visible at the bottom of the screen and when time is up it will say if blue or purple won the game. 
To make the game work it took a lot of different if-statements, different variables, for-loops and an understanding of how everything plays together and affects each other. 

**Describe how the object is programmed:**

I created a class called Heart and gave it only three constructors as I new this would be a challenging MiniEx. It was the x- and y-position and speed, where speed is 
set to be a random between 1 and 5. 
I made two functions within the class, move and show. Show() is what you see while move() is where I wrote which way the hearts should go and when they should do it etc. 
Within the game this class is the main character and what you need to catch. I used dist() to create the illusion that the people was catching the hearts and made them 
disappear with hearts.splice().

![MiniEx5](/public/MiniEx6/screenshot_game.png "miniex6")

**The characteristics of object-oriented programming and the wider implications**

Object-oriented programming is programming where you think of a piece of code as its own. It is similar to creating your own function except when you create an 
object with a class you have the possibility of having different attributes, movements, looks, things it can do etc. which can all be manipulated. It is a way of 
coding that can be very beneficial if it is a bigger program or if you are several people working on the same project. 

When you make an object, it will never be neutral – meaning that it will always have a set of attributes, things it can do or ways it look, that have been chosen 
by someone – usually the programmer. As programmers we can never be 100 percent neutral and objective which means that we can put things out in the world that can 
end up having a larger meaning. Our abstraction of something might be made based on something very specific and when it meets the world it might be taken out of that 
context, be skewed or misunderstood. A small choice, like what colour your object has, can have bigger implications than first initiated. Everything can be an object: 
animals, humans, shapes, things etc. Every decision about the characteristics of an object defines it and quantifies it down to those few characteristics. In the end 
these few characteristics can shape the users view on reality and shape what we understand as normal. 
Maybe these small tasks we are given each week seems insignificant but having a critical mindset about how we simplify and turn very complex things into concrete models 
and objects is an important lesson to remember when working in programming I believe. 

**Extend/connect your game to a wider digital culture context**

When having to think of a digital example my mind flies directly to The Sims. Although, today their sims-people are more complex and can do more things than 
ever before they are still lacking and will always be. Even though there are hundreds of hairdos to choose from they don’t have all and this can be said for 
every single characteristic and operation you can choose in this game. They have tried their best to abstract the most common characteristics of human beings 
into their game to make it as life-like as possible. I find it quite interesting to think of because there are so many things missing, both in looks but also 
the complexity of ones personality is boiled down to 50 different moods you can choose from. The Sims is a good example of a company that has to think long and 
hard about every single choice they make because their user base is so big and a large amount of them are young people who easily will reflect their own realities 
onto the game and think of it as normal. It is a game where the line between reality and fiction can get quite blurry – because who have not tried to make their own 
family on there?! Reflecting your own reality in a game can both be fun but also risky as you might end up thinking it is okay to just say sorry 10 times and then 
your husband is no longer mad at you for cheating? (fictional example just to be clear ;) ) – being critical to the differences between online/games/virtual world 
vs reality is important both as programmer and user. 
