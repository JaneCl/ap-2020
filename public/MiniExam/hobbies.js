//the four hobbies are made in p5*js - each function is connected to its own button

function fitness() {

  //badminton ketcher
  fill("LightSteelBlue")
  ellipse(500,280,30,50)
  fill(255)
  ellipse(500,280,25,45)
  fill("Yellow")
  rect(498.5,304,3,35,10)
  fill(0)
  rect(498,325,4,15,10)
  rect(505,260,1,40,10)
  rect(494,260,1,40,10)
  rect(499.5,258,1,45,10)
  rect(490,270,20,1,10)
  rect(490,290,20,1,10)
  rect(488,280,25,1,10)


  //badminton ball
  fill(200,200,200);
  strokeWeight(3)
  ellipse(530,320,5,6)
  ellipse(528,325,5,10)
  ellipse(532,325,5,10)
  ellipse(530,325,4,10)
  fill("Red")
  rect(527.5,320,5,2,1)

  //football
  push();
  translate(-30,0);
  strokeWeight(1)
  fill("SaddleBrown")
  ellipse(600,300,40,80)
  fill(255)
  rect(598,260,5,80,10)
  rect(591,290,20,5,10)
  rect(591,300,20,5,10)
  rect(591,310,20,5,10)
  rect(591,320,20,5,10)
  rect(584,270,32,10,10)
  pop();
}

function foodie() {
  //Pan
  fill(0);
  ellipse(600,200,80,80)
  fill(50)
  ellipse(600,200,60,60)

  fill(0);
  rect(595,230,10,50,10);

  fill(255)
  strokeWeight(0.1)
  ellipse(595,210,35,25)
  ellipse(605,195,35,25)
  fill(255, 204, 0)
  ellipse(605,195,10,10)
  ellipse(595,210,10,10)

  //Spoon
  push();
  translate(-40,15);
  strokeWeight(1.5)
  fill("SaddleBrown")
  rect(695,210,10,50,10)
  ellipse(700,190,35,60)
  fill(163, 111, 26);
  ellipse(700,190,22,40)
  pop();
}

function couchPotato() {
  //Netflix
  //top
  fill(300)
  line(560, 120, 510,20);
  line(590, 20, 540,100);
  arc(550, 100, 90, 80, 135, 135, OPEN);
  //TV Body
  fill(50)
  rect(300, 100, 500, 400, 50);
  //screen
  fill(150,0,0)
  rect(350, 140, 390, 320, 50);
  fill(230)
  rect(425, 500, 250, 20, 20, 15, 10, 5);
  fill(230)
  ellipse(750, 460, 40, 40);
  rect(750,350,32,6)
  rect(750,320,32,6)
  rect(750,290,32,6)
  rect(750,260,32,6)

  fill(255)
  textSize(100);
  textAlign(CENTER);
  text('Netflix',550, 325);
  strokeWeight(12.0);
  strokeCap(ROUND)
}


function creative() {
  //brush and pan
  noStroke()
  fill("SaddleBrown")
  ellipse(600,300, 120,120)
  fill("SaddleBrown")
  ellipse(600,220,100,80)
  fill(255)
  rect(580,310,40,20,10)
  ellipse(600,250,30,30)
  fill("GoldenRod")
  rect(590,90,20,140,10)
  fill("SaddleBrown")
  rect(585,90,30,40,10)
  fill(0)
  ellipse(578,100,10,10)
  ellipse(578,115,10,10)
  ellipse(622,100,10,10)
  ellipse(622,115,10,10)
  rect(595,100,1,240,10)
  rect(597,100,1,240,10)
  rect(599,100,1,240,10)
  rect(601,100,1,240,10)
  rect(603,100,1,240,10)

  //Guitar
  fill("DarkGoldenRod")
  arc(600, 300, 90, 90, 105, PI + QUARTER_PI, PIE);
  fill(255)
  ellipse(605,310,15,15)
  fill("Red")
  ellipse(605,330,15,15)
  fill("Blue")
  ellipse(580,320,15,15)
  fill("Green")
  ellipse(630,310,15,15)
  fill("Yellow")
  ellipse(620,285,15,15)
  fill("Pink")
  ellipse(580,298,15,15)
  fill("Brown")
  rect(660,290,10,50,10)
  fill(10)
  triangle(655,270,675 ,270,665,300);
  fill("Peru")
  rect(660,290,10,10,10)
}
