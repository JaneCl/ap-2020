//All images are preloaded

function preload() {
  font = loadFont('font/Quicksand.otf');

  //emojis telling whether your choice of gender, skin colour, sexuality, profession and hobby is good or bad
  bad_emoji = loadImage("Assets/bad_emoji.png");
  good_emoji = loadImage("Assets/good_emoji.PNG");

  //gender
  male = loadImage("Assets/Man_Start.png");
  female = loadImage("Assets/Woman_Start.png");

  //gender + skin colour
  maleblack = loadImage("Assets/Man_Black.png");
  maleyellow = loadImage("Assets/Man_yellow.png");
  malewhite = loadImage("Assets/Man_white.png");
  malebrown = loadImage("Assets/Man_brown.png");
  femaleblack = loadImage("Assets/Woman_black.png");
  femaleyellow = loadImage("Assets/Woman_yellow.png");
  femalewhite = loadImage("Assets/Woman_white.png");
  femalebrown = loadImage("Assets/Woman_brown.png");

  //gender + skin colour + heterosexual
  maleblackhetero = loadImage("Assets/Man_Black_hetero.png");
  maleyellowhetero = loadImage("Assets/Man_yellow_hetero.png");
  malewhitehetero = loadImage("Assets/Man_white_hetero.png");
  malebrownhetero = loadImage("Assets/Man_brown_hetero.png");
  femaleblackhetero = loadImage("Assets/Woman_black_hetero.png");
  femaleyellowhetero = loadImage("Assets/Woman_yellow_hetero.png");
  femalewhitehetero = loadImage("Assets/Woman_white_hetero.png");
  femalebrownhetero = loadImage("Assets/Woman_brown_hetero.png");

  //gender + skin colour + homosexual
  maleblackhomo = loadImage("Assets/Man_black_homo.png");
  malebrownhomo = loadImage("Assets/Man_brown_homo.png");
  malewhitehomo = loadImage("Assets/Man_white_homo.png");
  maleyellowhomo = loadImage("Assets/Man_yellow_homo.png");
  femaleblackhomo = loadImage("Assets/Woman_black_homo.png");
  femalebrownhomo = loadImage("Assets/Woman_brown_homo.png");
  femalewhitehomo = loadImage("Assets/Woman_white_homo.png");
  femaleyellowhomo = loadImage("Assets/Woman_yellow_homo.png");

  //gender + skin colour + heterosexual + doctor
  maleblackheterodoctor = loadImage("Assets/Man_black_doktor.png");
  malebrownheterodoctor = loadImage("Assets/Man_brown_doktor.png");
  malewhiteheterodoctor = loadImage("Assets/Man_white_doktor.png");
  maleyellowheterodoctor = loadImage("Assets/Man_yellow_doktor.png");
  femaleblackheterodoctor = loadImage("Assets/Woman_black_doktor.png");
  femalebrownheterodoctor = loadImage("Assets/Woman_brown_doktor.png");
  femalewhiteheterodoctor = loadImage("Assets/Woman_white_doktor.png");
  femaleyellowheterodoctor = loadImage("Assets/Woman_yellow_doktor.png");

  //gender + skin colour + homosexual + doctor
  maleblackhomodoctor = loadImage("Assets/Man_black_homo_doktor.png");
  malebrownhomodoctor = loadImage("Assets/Man_brown_homo_doktor.png");
  malewhitehomodoctor = loadImage("Assets/Man_white_homo_doktor.png");
  maleyellowhomodoctor = loadImage("Assets/Man_yellow_homo_doktor.png");
  femaleblackhomodoctor = loadImage("Assets/Woman_black_homo_doktor.png");
  femalebrownhomodoctor = loadImage("Assets/Woman_brown_homo_doktor.png");
  femalewhitehomodoctor = loadImage("Assets/Woman_white_homo_doktor.png");
  femaleyellowhomodoctor = loadImage("Assets/Woman_yellow_homo_doktor.png");

  //gender + skin colour + heterosexual + CEO
  maleblackheteroCEO = loadImage("Assets/Man_black_CEO.png");
  malebrownheteroCEO = loadImage("Assets/Man_brown_CEO.png");
  malewhiteheteroCEO = loadImage("Assets/Man_white_CEO.png");
  maleyellowheteroCEO = loadImage("Assets/Man_yellow_CEO.png");
  femaleblackheteroCEO = loadImage("Assets/Woman_black_CEO.png");
  femalebrownheteroCEO = loadImage("Assets/Woman_brown_CEO.png");
  femalewhiteheteroCEO = loadImage("Assets/Woman_white_CEO.png");
  femaleyellowheteroCEO = loadImage("Assets/Woman_yellow_CEO.png");

  //gender + skin colour + homosexual + CEO
  maleblackhomoCEO = loadImage("Assets/Man_black_homo_CEO.png");
  malebrownhomoCEO = loadImage("Assets/Man_brown_homo_CEO.png");
  malewhitehomoCEO = loadImage("Assets/Man_white_homo_CEO.png");
  maleyellowhomoCEO = loadImage("Assets/Man_yellow_homo_CEO.png");
  femaleblackhomoCEO = loadImage("Assets/Woman_black_homo_CEO.png");
  femalebrownhomoCEO = loadImage("Assets/Woman_brown_homo_CEO.png");
  femalewhitehomoCEO = loadImage("Assets/Woman_white_homo_CEO.png");
  femaleyellowhomoCEO = loadImage("Assets/Woman_yellow_homo_CEO.png");


  //gender + skin colour + heterosexual + service worker
  maleblackheteroservice = loadImage("Assets/Man_black_service.png");
  malebrownheteroservice = loadImage("Assets/Man_brown_service.png");
  malewhiteheteroservice = loadImage("Assets/Man_white_service.png");
  maleyellowheteroservice = loadImage("Assets/Man_yellow_service.png");
  femaleblackheteroservice = loadImage("Assets/Woman_black_service.png");
  femalebrownheteroservice = loadImage("Assets/Woman_brown_service.png");
  femalewhiteheteroservice = loadImage("Assets/Woman_white_service.png");
  femaleyellowheteroservice = loadImage("Assets/Woman_yellow_service.png");

  //gender + skin colour + homosexual + service worker
  maleblackhomoservice = loadImage("Assets/Man_black_homo_service.png");
  malebrownhomoservice = loadImage("Assets/Man_brown_homo_service.png");
  malewhitehomoservice = loadImage("Assets/Man_white_homo_service.png");
  maleyellowhomoservice = loadImage("Assets/Man_yellow_homo_service.png");
  femaleblackhomoservice = loadImage("Assets/Woman_black_homo_service.png");
  femalebrownhomoservice = loadImage("Assets/Woman_brown_homo_service.png");
  femalewhitehomoservice = loadImage("Assets/Woman_white_homo_service.png");
  femaleyellowhomoservice = loadImage("Assets/Woman_yellow_homo_service.png");


  //gender + skin colour + heterosexual + construction worker
  maleblackheteroconstructor = loadImage("Assets/Man_black_constructor.png");
  malebrownheteroconstructor = loadImage("Assets/Man_brown_constructor.png");
  malewhiteheteroconstructor = loadImage("Assets/Man_white_constructor.png");
  maleyellowheteroconstructor = loadImage("Assets/Man_yellow_constructor.png");
  femaleblackheteroconstructor = loadImage("Assets/Woman_black_constructor.png");
  femalebrownheteroconstructor = loadImage("Assets/Woman_brown_constructor.png");
  femalewhiteheteroconstructor = loadImage("Assets/Woman_white_Constructor.png");
  femaleyellowheteroconstructor = loadImage("Assets/Woman_yellow_constructor.png");

  //gender + skin colour + homosexual + construction worker
  maleblackhomoconstructor = loadImage("Assets/Man_black_homo_constructor.png");
  malebrownhomoconstructor = loadImage("Assets/Man_brown_homo_constructor.png");
  malewhitehomoconstructor = loadImage("Assets/Man_white_homo_constructor.png");
  maleyellowhomoconstructor = loadImage("Assets/Man_yellow_homo_constructor.png");
  femaleblackhomoconstructor = loadImage("Assets/Woman_black_homo_constructor.png");
  femalebrownhomoconstructor = loadImage("Assets/Woman_brown_homo_constructor.png");
  femalewhitehomoconstructor = loadImage("Assets/Woman_white_homo_constructor.png");
  femaleyellowhomoconstructor = loadImage("Assets/Woman_yellow_homo_constructor.png");

}
