//Exam Project - "SoNotMe"
//Made by Julie Spangaard Steffensen, Magnus Bak, Mikkel Holger Nørgaard Lundager and Jane Clausen
//Confirmed running on chrome browser (Version 81.0.4044.129, 64bit)

//variables used for buttons - each array is a category
let gender = ["male","female"];
let skinColour = ["skinYellow","skinBlack","skinBrown","skinWhite"];
let sexuality = ["heterosexual","homosexual"];
let profession = ["doctor","service","director","construction"];
let hobby = ["food","creative","couchPotato","fitness"];
//refresh button
let refresh;

//the variables determining from each category
let sex = "";
let skin = "";
let orientation = "";
let job = "";
// name of images
let pngName;


function setup() {
  createCanvas(windowWidth,windowHeight);
  background(159,212,237);

  //calling all buttons, the pokemon card and menu
  reDraw();
}



function reDraw() {
  //Pokemon card
    noStroke();
    fill(159,162,166,50);
    rect(160,85,510,610,5);

    //card
    fill(211,222,235);
    rect(150,75,500,600,5);

    //avatar
    fill(255,255,255);
    stroke(195,213,222);
    strokeWeight(2);
    rect(160,85,480,450,50);

    //score board
    rect(160,550,480,115,30);

    //your avatar
    strokeWeight(2);
    rect(200,50,400,60,10);

    //text "Your Avatar"
    textSize(40);
    textFont(font);
    stroke(32, 114, 230);
    strokeWeight(3);
    fill(32, 114, 230);
    text("So", 300,95);
    text("Me", 430,95);
    stroke(250, 136, 42);
    fill(250, 136, 42);
    text("Not", 355,95);


  //Avatar chooser menu
    noStroke();
    //shadow
    noStroke();
    fill(159,162,166,50);
    rect(860,85,510,610,5);
    //menu-background
    fill(211,222,235);
    rect(850,75,500,600,5);
    //boxes determining category
    stroke(255,200)
    rect(857,95,488,70,5);
    rect(857,191,488,80,5);
    rect(857,291,488,80,5);
    rect(857,394,488,165,5);
    rect(857,568,488,80,5);
    noStroke();

  //Buttons for menu
    //gender
    gender[0] = createButton('male');
    gender[0].position(1100-150,100);
    gender[0].mousePressed(maleStart);
    gender[0].class('styleMale');

    gender[1] = createButton('female');
    gender[1].position(1100+50,100);
    gender[1].mousePressed(femaleStart);
    gender[1].class('styleFemale');

    //skin colour
    skinColour[0] = createButton('yellow');
    skinColour[0].position(1100 + 10,200);
    skinColour[0].mousePressed(changeSkinYellow);
    skinColour[0].class('styleButtons');

    skinColour[1] = createButton('black');
    skinColour[1].position(1100 + 125,200);
    skinColour[1].mousePressed(changeSkinBlack);
    skinColour[1].class('styleButtons');

    skinColour[2] = createButton('brown');
    skinColour[2].position(1000 - 10,200);
    skinColour[2].mousePressed(changeSkinBrown);
    skinColour[2].class('styleButtons');

    skinColour[2] = createButton('white');
    skinColour[2].position(880,200);
    skinColour[2].mousePressed(changeSkinWhite);
    skinColour[2].class('styleButtons');

    //sexuality
    sexuality[0] = createButton('heterosexual');
    sexuality[0].position(1100 - 175,300);
    sexuality[0].mousePressed(changeSexualityHetero);
    sexuality[0].class('styleHetero');

    sexuality[1] = createButton('homosexual');
    sexuality[1].position(1100 + 25,300);
    sexuality[1].mousePressed(changeSexualityHomo);
    sexuality[1].class('styleHomo');

    //profession
    profession[0] = createButton('doctor');
    profession[0].position(1100 + 10,400);
    profession[0].mousePressed(changeProfessionDoctor);
    profession[0].class('styleDoctor');

    profession[1] = createButton('service worker');
    profession[1].position(1100 - 200,490);
    profession[1].mousePressed(changeProfessionService);
    profession[1].class('styleService');

    profession[2] = createButton('CEO');
    profession[2].position(1100 - 120,400);
    profession[2].mousePressed(changeProfessionCEO);
    profession[2].class('styleCEO');

    profession[3] = createButton('construction worker');
    profession[3].position(1100,490);
    profession[3].mousePressed(changeProfessionConstructor);
    profession[3].class('styleConstructor');

    //hobbies
    hobby[0] = createButton('eater');
    hobby[0].position(1100 - 240,575);
    hobby[0].mousePressed(hobbyFood);
    hobby[0].class('styleFoodie');

    hobby[1] = createButton('creative');
    hobby[1].position(1100 - 135,575);
    hobby[1].mousePressed(hobbyCreative);
    hobby[1].class('styleCreative');

    hobby[2] = createButton('couchPotato');
    hobby[2].position(1100 - 17,575);
    hobby[2].mousePressed(hobbyCouch);
    hobby[2].class('styleCouchpotato');

    hobby[3] = createButton('fitness');
    hobby[3].position(1100 + 140,575);
    hobby[3].mousePressed(hobbyFitness);
    hobby[3].class('styleFitness');

    //refresh page
    refresh = createButton('new me')
    refresh.position(185,575);
    refresh.mousePressed(reDraw);
    refresh.class('styleButtons');
}

//function that makes sure we load the correct image depending on the choices made/buttons pressed
function loadpng() {

  //if skin is not ""(makes it go through options) then skin is _(for the image file name) + skincolour of button
  //so if the variable skin is not empty (ie there is a colour) then the variable skin should be that colour
  if (skin != "") {
    skin = skin
  }
  if (orientation != "") {
    orientation = orientation
  }
  if (job != "") {
    job = job
  }

  let pngName = sex + skin + orientation + job;


  print(pngName);
  eval(pngName + ".resize(400,300)");
  eval("image(" + pngName + ",200,200)");


}



function femaleStart() {
  //making skin, orientation and job empty the picture will revert back to just female without any add-ons
  skin = "";
  orientation = "";
  job = "";
  sex = "female";

  //this rect is to make sure nothing overlaps (so that the hobbies disappear if you choose to go back to an earlier category)
  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  bad_emoji.resize(50,50);
  image(bad_emoji,330,580);
}

function maleStart() {
  skin = "";
  orientation = "";
  job = "";
  sex = "male";

  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  good_emoji.resize(50,50);
  image(good_emoji,330,580);
}


function changeSkinBlack() {
  job = "";
  orientation = "";
  skin = "black";

  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  bad_emoji.resize(50,50);
  image(bad_emoji,385,580);
  }

function changeSkinYellow() {
  job = "";
  orientation = "";
  skin = "yellow";

  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  good_emoji.resize(50,50);
  image(good_emoji,385,580);
  }

function changeSkinBrown() {
  job = "";
  orientation = "";
  skin = "brown";

  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  bad_emoji.resize(50,50);
  image(bad_emoji,385,580);

  }

function changeSkinWhite() {
  job = "";
  orientation = "";
  skin = "white";

  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  good_emoji.resize(50,50);
  image(good_emoji,385,580);
  }

function changeSexualityHetero() {
  job = "";
  orientation = "hetero";

  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  good_emoji.resize(50,50);
  image(good_emoji,440,580);
}

function changeSexualityHomo() {
  job = "";
  orientation = "homo";

  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  bad_emoji.resize(50,50);
  image(bad_emoji,440,580);
}


function changeProfessionDoctor() {
  job = "doctor";

  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  good_emoji.resize(50,50);
  image(good_emoji,495,580);
}

function changeProfessionCEO() {
  job = "CEO";

  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  good_emoji.resize(50,50);
  image(good_emoji,495,580);
}

function changeProfessionService() {
  job = "service";

  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  bad_emoji.resize(50,50);
  image(bad_emoji,495,580);
}

function changeProfessionConstructor() {
  job = "constructor"
  fill(255);
  rect(160,130,480,400,50);

  loadpng();
  //if loadpng happens, then emoji appears
  bad_emoji.resize(50,50);
  image(bad_emoji,495,580);
}


function hobbyFitness() {
  push();
  translate(30,160);
  fitness();
  pop();

  good_emoji.resize(50,50);
  image(good_emoji,550,580);
}

function hobbyFood() {
  push();
  translate(-80,-20);
  foodie();
  pop();

  bad_emoji.resize(50,50);
  image(bad_emoji,550,580);
}

function hobbyCouch() {
  push();
  translate(115,110);
  scale(0.25);
  couchPotato();
  pop();

  bad_emoji.resize(50,50);
  image(bad_emoji,550,580);
}

function hobbyCreative() {
  push();
  translate(-50,340);
  scale(0.5);
  creative();
  pop();

  good_emoji.resize(50,50);
  image(good_emoji,550,580);

}
