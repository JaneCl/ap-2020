//let is a function used when you want to _let_ something be somthing specific it becomes the rule
//let there be a diameter and let the angle be = 0
let diameter;
let angle = 0;

function setup() {
  // put setup code here in the following:
  createCanvas(700,500);
  //diameteren of the ellipsis
  diameter = 70;

}
function draw() {
  // put drawing code here

  //made the background yellow
  background(255, 204, 100);

  //the forms to make the ellipsis have movement
    let d1 = 10 + (sin(angle) * diameter) / 2 + diameter;
    let d2 = 10 + (sin(angle + PI / 2) * diameter) / 2 + diameter / 2;
    let d3 = 10 + (sin(angle + PI) * diameter) / 2 + diameter / 2;
    let d4 = 10 + (sin(angle + PI/4) * diameter) / 2 + diameter /2;

  //ellipse at the top
  fill(127,0,0);
  stroke(127,0,0);
  ellipse(350,100,d1,d1);
  //ellipse to the right
  fill('rgba(90%,0%,50%,0.5)');
  stroke('rgba(90%,0%,50%,0.5)');
  ellipse(500,250,d2,d2);
  //third ellipse - to the left
  fill(255,0,0);
  stroke(255,0,0);
  ellipse(200,250,d3,d3);
  //fourth ellipse - bottom
  fill(255,200,200);
  stroke(255,200,200);
  ellipse(350,400,d4,d4);
  //in between those four are the rest of the ellipsis
  fill(255,200,200);
  stroke(255,200,200);
  ellipse(420,140,d4,d4);
  fill(255,150,100);
  stroke(255,150,100);
  ellipse(475,190,d3,d3);
  fill(255,100,140);
  stroke(255,100,140);
  ellipse(285,140,d2,d2);
  fill(255,50,100);
  stroke(255,50,100);
  ellipse(230,190,d1,d1);
  fill(255,120,120);
  stroke(255,120,120);
  ellipse(420,365,d4,d4);
  fill(255,150,140);
  stroke(255,150,140);
  ellipse(475,315,d1,d1);
  fill(255,160,100);
  stroke(255,160,100);
  ellipse(285,365,d2,d2);
  fill(127,0,10);
  stroke(127,0,10);
  ellipse(230,315,d3,d3);

    //angle is the speed of the movement
    angle += 0.01;
}
