<h2> Mini Exercise 1 - My very First Program </h2>

RUNME MiniEx1: https://janecl.gitlab.io/ap-2020/MiniEx1/ - I've had quite a few problems with it, so if it does not work please please copy+paste the coding I've done and
open it through your own programming software (it is a moving piece)

![Moving bubbles](MiniX1 - screenshot.png)

This first mini exercise were to get us familiarised with coding. 
Although the biggest struggles seemed to be everything beyond it - understanding and making GitKraken work, how to set up Atom with the correct folders,
trying to make the URL work etc. 

The actual coding experience were very rewarding especially when looking back to where I started just a week ago 'till now, and how daunting a page of code 
looked then compared to now. 
To get some inspiration on what to make I looked at [Saskia Freeke's twitter](https://twitter.com/sasj_nl) which made me want to try and make shapes that would stay 
in place and get bigger and smaller in a loop. Making shapes move in that particular way turned out to be the hardest part of my code. 

Before making anything move I wanted to place my circles, which was a case of understanding the upside down coordinates that is the canvas. I found drawing it out
on a piece of paper helped a lot as I was placing quite a few ellipses. I used the following code for every single ellipses:

ellipses(x,y,w,(h))

After getting the right placements it was all about the colours of the background and circles. This was a case of changing out numbers and figuring out what each
one did in terms of shading and darkness/coolnes of the colours. 

Then came the more challenging bit of my idea - making every circle move and even better if it were in different tempos. First I found 
an [example of circles moving on p5.js' website](https://p5js.org/examples/math-sine.html). Here they were using sinus, to make the movement happen.
My initial thought was "oh easy, just copy paste!" - this turned out not to be the case. 
To make the code work on my own circles I had to have at least a bit of understanding in what the different parts of the code did and why they were there.
This was a long process of deleting or changing parts only to see what they did and then when a part was figured out applying it to my own circles. I have tried my best
to make usable comments for my own sake to try and remember what the different parts were used for. 

Coding is very rewarding in the sense that you get to play around with numbers and different codes and how you understand the meaning of the individual factors of a single code
by changing out numbers or the order of codes and thereby understanding the syntax of a code and what impact it has on other codes. I can already now say
that I think of code in a different way - I can look at a piece and get curious as to how that piece is coded. Furthermore reading code is a lot less daunting now,
there is a lot I do not understand (even in my own project) but having a little understanding for the syntax and knowing that maybe you can figure out the meaning of a 
code by either playing with it or simply look it up is reassuring. Trying to modify a code like the sine code I did in this project is very educational as well as writing it
myself. 

I found coding to be much more a case of thinking logically than thinking it as a language. I imagine this might be different to everybody as we think 
very differently. 
The way coding is similar to reading and writing is especially the syntax - the way we understand what is being said. In terms of writing code it is quite different 
because of the foreign language - as if you are learning all over again. Also it seems to be a mixture of language and maths which is a form of syntax that needs a bit of
getting used to. 

For me coding and programming in general are recepies or instructions for the hardware to follow. I've always thought of it in terms of apps, websites and electronics in 
general. But never as art or a way to express yourself which I'm pretty excited about! This weeks assigned readings opened my eyes to the possibilites there is in coding and 
showed how instrumental it is in our everyday lives by explaining programming as a literacy just like reading, writing and maths. 
