**Links to each flowchart:**

[Phone-story game](https://gitlab.com/JaneCl/ap-2020/-/blob/master/public/MiniEx10/iPhone-game-flowchart.pdf)

[Avatar game](https://gitlab.com/JaneCl/ap-2020/-/blob/master/public/MiniEx10/Avatar-flowchart.pdf)

[MiniEx6 flowchart – catching hearts game](https://gitlab.com/JaneCl/ap-2020/-/blob/master/public/MiniEx10/Flowchart%20-%20hearts%20game%20MiniEx6.pdf)

Link to repository: https://gitlab.com/JaneCl/ap-2020/-/tree/master/public/MiniEx10

**What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?**

The difference between making a flowchart of an idea and making a flowchart for a complex already made program was huge. Initially I thought making an understandable 
simplistic flowchart would be more difficult than doing a complex one because you must take into consideration how a non-coder would understand what you are trying to 
say with the flowchart. Even though I’m still very new to the world of coding the language very quickly rubbed off on me, so when trying to explain something to eg. 
My mother or roommate it is very difficult not to use the terms that I’ve learnt. For this reason, I thought making a simplistic flowchart could very well be quite 
challenging. In the end it was not too difficult with the help of my group but being conscious of how each statement is being phrased is definitely very important 
when mapping out an initial idea where every person on a bigger project should be able to understand the same work plan. 

When working with my own flowchart that is covering my program from MiniEx6 which is a ‘catching hearts game’ I found the balance between understandability and complexity 
pretty hard as I wanted it to be both true to the coded sketch but also what happens visually when playing the game. My biggest hurdle to overcome was figuring out which 
order to make the different events happening as when playing the game several things happens at once: there is a timer, there is a scoreboard, there are 6 hearts and two 
players. Somehow all of these had to be put in an understandable and logical order and at the same time explained so that a programmer can imagine how this would look like
coded but also how it would look visually. My flowchart ended up being not too complicated in terms of algorithmic procedures because I think the conceptual understanding
would have been lost. If this project should be written out completely with algorithmic procedures, I would have made several flowcharts for different parts of the game
that then in the end could be collected to one. 

With my limited experience with flowcharts I think a good mixture of simplicity and complexity is good – I imagine if you have a big project you would create a very 
simple (in terms of the language used) flowchart to map out all the things happening in the project and then maybe creating several smaller much more complex flowcharts
for the different specific things happening within the program and this would be written out with much more algorithmic structure. 

**What are the technical challenges for the two ideas and how are you going to address them?**
Our first idea is a game of sorts that has a storyline told through a phone. You get a message from an unknown number and can choose to reply with different options. 
In the end you realise you have been hacked or attacked by a virus by continuing to answer the questions from the stranger. 

The challenges in doing this program would be the many options of replies you can have and the intricate developments of the different stories. I imagine the best 
solution would be creating two, if not more, .json-files, one being the replies you can choose from and another being the messages you get from the virus. 

Our second idea is a ‘create an avatar’ game – which is set up very much like pokemon cards – so when choosing gender, sexuality, employment, clothing etc. your
avatar gets a score that will be displayed underneath it – this is a comment on the stereotypes exciting in the world of who is more powerful, who has the most 
friends, money etc. and what that does to your ‘value as a human being’. 

The technical challenge in this one is going to be how you change the attributes of your avatar, so you can modify it as you wish. I have tried in a previous 
MiniEx’s to create the illusion of removing elements from a program again and found it quite hard when you have several other things on the screen you do not want
to remove. This will also be a quite complex code in terms of all the options that there is going to be – this will probably need some classes, .json-files and many 
if statements and mousePressed. 


**What is the value of the individual and the group flowchart that you have produced?**
The value of the flowcharts made with my group is we now all have a conceptually good idea of how both ideas is supposed to be carried out. 
This means less misunderstandings and confusions about what happens when. That is definitely the strength of a simplistic and communicative flowchart 
as it help eliminate doubts in a program. 

The value of my own flowchart which is more complex and heavier with algorithmic procedures is great for understanding the wheels behind the clock.
Even though it could have been a lot more complex and intricate the flowchart is great for a programmer to conceptualize and imagine how this can be carried out in code. 
