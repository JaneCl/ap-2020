[Link to MiniEx3 - throbber](https://janecl.gitlab.io/ap-2020/MiniEx3/)

[Link to code/repository](https://gitlab.com/JaneCl/ap-2020/-/blob/master/public/MiniEx3/sketch.js)

## MiniEx 3 - the throbber ##

So, this week the end result is far from my original idea as it turned out to be quite a bit more difficult than first anticipated. 
Well, you learn the restraints of coding and time management by trial and error I suppose. This week we had to reflect upon temporality in digital 
culture through the throbber icon and my inspiration came from Winnie Soon’s installation piece [Throb]( http://siusoon.net/throb/) with all the lines at 
different angles. I wanted that to be my background but also making the lines move around themselves at various speeds. Then I wanted some of the lines to spell out __WAIT__ 
also making these lines move around themselves, but then when they were at a right angle they would stop and change colour and thereby spell out ‘wait’. This turned out to be 
pretty difficult to do as I made for-loop to create the many lines in the background and I found out that you cannot make if-statements in a for-loop which was how I originally 
thought I would make everything rotate on its own axis by saying “if the coordinate is a,b and c then rotate this fast and this way etc.”. So, in stead to spell out wait I 
had to place lines on top of the background one’s to then try and make those rotate. I’m not sure why, but it seems impossible to make so many lines at once rotate around 
themselves and not just one centered axis (which is what the end result is). I’ve also experimented with making each letter rotate around itself, but I could not succeed 
in that either. This is also why I’ve made a new function for each letter because my theory was, that I could make a whole function rotate (again did not succeed). 

To make ‘wait’ rotate around itself I found it easiest to use WEBGL and then the syntax rotate(millis() /1000)); The reason for this was to create the feeling of time passing. 
As if it was an analog watch or any other throbber that goes round and round. The time concept apparent for humans is millisecond/1000 and I did not change the frameCount – which 
could be thought of as a computer’s perception of time (60 frames per second) as they don’t have one. 

![screenshot of throbber](public/MiniEx3/screenshot_minix3.png)

In Winnie Soon’s text *Throbber: Executing Micro-Temporal Streams* she talks about the thobber as a good visual icon on something happening and time passing. 
How it symbolizes the data streams happening ‘behind’ the throbber and everything that the everyday user might not know a whole lot about. The throbber is a 
cultural phenomenon that operates within a live computational environment to communicate to the user that something is processing. Progress is happening while time passes. 

A throbber represents disruption in your flow and use, and the fact that it almost always is an animation of some sort emphasizes time passing which for many 
is frustrating and annoying. It communicates lack of network or that the file/video is quite big. My intent with the throbber design was to make something interesting 
that took time to figure out so that the time spent waiting would feel shorter as you would be entertained for a short while. Although in reality I don’t think it turned 
out that way, but it does seem to be a bit different from the typical throbber. 

