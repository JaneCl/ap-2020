//Program for MiniEx3 - throbbers
//wanted to try out loops and experiment with time
//to make 'WAIT' rotate in this way I had to add WEBGL for a third dimension
//Made by JaneC

function setup() {
  createCanvas(950,700,WEBGL);
}

function draw() {
  background(255, 204, 100);

//two for-loops to create the lines in the background
  for(var x = -475; x < width; x += 25){
    for(var y = -350; y < height; y +=25){
          noStroke();
          fill(250, 177, 30);
          rect(x,y,2,20);
    }
  }

  //making the letters rotate
  rotate(millis()/1000);

  //I'd made the letters up in one corner. So to center is I used translate
  translate(-360, -85)

  //colour of text
  fill(222, 94, random(255));

  //calling my homemade functions
  theW();
  theA();
  theI();
  theT();

//I made functions for each letter
//the 'W'
function theW() {
  rect(35,35,2,20);
  rect(35,60,2,20);
  rect(35,85,2,20);
  rect(60,110,2,20);
  rect(85,135,2,20);
  rect(110,110,2,20);
  rect(110,85,2,20);
  rect(135,135,2,20);
  rect(160,110,2,20);
  rect(185,85,2,20);
  rect(185,60,2,20);
  rect(185,35,2,20);
}
//the 'A'
function theA() {
  rect(235,135,2,20);
  rect(235,110,2,20);
  rect(260,85,2,20);
  rect(285,60,2,20);
  rect(310,35,2,20);
  rect(335,60,2,20);
  rect(360,85,2,20);
  rect(385,110,2,20);
  rect(385,135,2,20);
  rect(260,110,2,20);
  rect(285,110,2,20);
  rect(310,110,2,20);
  rect(335,110,2,20);
  rect(360,110,2,20);
}
//the 'I'
function theI() {
  rect(435,35,2,20);
  rect(460,35,2,20);
  rect(485,35,2,20);
  rect(510,35,2,20);
  rect(535,35,2,20);
  rect(485,60,2,20);
  rect(485,85,2,20);
  rect(485,110,2,20);
  rect(435,135,2,20);
  rect(460,135,2,20);
  rect(485,135,2,20);
  rect(510,135,2,20);
  rect(535,135,2,20);
}
//the 'T'
function theT() {
  rect(585,35,2,20);
  rect(610,35,2,20);
  rect(635,35,2,20);
  rect(660,35,2,20);
  rect(685,35,2,20);
  rect(635,60,2,20);
  rect(635,85,2,20);
  rect(635,110,2,20);
  rect(635,135,2,20);
}
}
