Link to MiniEx7 - [A Generative Program](https://janecl.gitlab.io/ap-2020/MiniEx7.1/)

Link to repository: https://gitlab.com/JaneCl/ap-2020/-/tree/master/public/MiniEx7.1


**Inspiration**
To get an idea of what to create this week I had a browse through the different sources Winnie provided and fell over the artwork Permutable on ReCode’s 
website: http://recodeproject.com/artwork/v1n3permutable 

![MiniEx7](/public/MiniEx7.1/permutable.png "miniex7")
 

I thought it could be interesting to see what happened when you turned a mural into a coded program and have a think about which rules would determine how 
the program would look. 

Rule 1: I want to create random shapes. 
Rule 2: I want the colors to be random, but within a few set of colours – ie every element has 25 percent chance of being a certain colour.

**Approach and thought**
To make this I created a square out of four triangles this was put into one function for easier access and better flow in the sketch. The function trisquare
was then added into a for loop in a for loop to make it fill the entire window. What’s important to note as well is adding the i & j into each argument in the 
triangle(); otherwise it will loop on top of itself. 
To create the four colour options for the triangles I made three arrays, r,g and b splitting up the colours so the fourth variable colorChoice(); could randomly 
choose one of the four colours.  

I know it is not a ‘growing’ or evolving generative program per se (like the 10Print that gets bigger and bigger or like the tree growing) but with every frame a 
new unique composition appears and the colours determine the shapes within the piece, which I find quite meditative to look at. As an afterthought I have also wondered 
if it would be more pleasing to look at if the frame count could be even lower, so each ‘mural’ had more time. 
To make the program even more complex and a truly generative program it might be possible to make it as if the triangles were folding out the art-piece one by one 
(for each frame a new triangle would appear, perhaps starting at the centre and then working it’s way out to the edges of the screen) 

![MiniEx7](/public/MiniEx7.1/screenshot_miniex7.1.png "miniex7")
 

**The Role of Rules**
The role of the rules and processes in the generative program is already touched upon as the rule of random is what shapes each frame into different unique 
shapes made out of triangles. What is also an important rule to be able to create the different shapes is the fact that there are only four colours to choose 
from – if it had been a fill(random(255), random(255), random(255)); the sketch would not have had the same effect at all as it would rarely be other shapes 
than the original triangles because each triangle would be a new colour. 

**Auto Generator**
Instructions and rules with unknown (random) variables is the base for a generative program. It’s what determines how the program evolves. 
Not just this mini exercise but also the lecture and chapter ‘Randomness’ 10 PRINT CHR$(205.5+RND(1)); : GOTO 10” by Nick Montford this week made me 
understand on a much deeper level how computers can never be truly random and how we in reality can discuss if anything ever is truly random?! (that is 
a discussion for another time as it can be a long one) Instead with computers you can talk about pseudo randomness as it pretends to be random, but because 
it is deterministic, controlled and works on logic and commands it would not be able to be random. Thereby the function random(); is one of the main functions 
to make the code seem less controlled than it is in reality. 
What distinguish a generative program is also the very humanlike behaviour that some programs have – this is a combination of the program evolving on its own seemingly very randomly and being very convincing at that. 
A consequence of computing is that the programmer gives up part of its control as the output is now decided by the computer itself – that is until you choose to change the code (if you do…)

