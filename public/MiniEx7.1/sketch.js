//MiniEx7 - A Generative Program
//Made by Jane C.


//a,b and c is making up the size of the triangle (using them as coodinates down in the triangles)
//to resize the triangles and still preserve the right shape a must always be half the sum of b (and not changing c)
let a = 40;
let b = 80;
let c = 0;

//These variables are used to create the array of colors and gives me the opportunity to make random colors within the array
let colorChoice;
let red = [247,245,89,81];
let green = [102,143,143,135];
let blue = [40,34,135,127];


function setup() {
  createCanvas(windowWidth,windowHeight);
  background(255);
  frameRate(1);
}


function draw() {
  //for loop 'copy & paste' the triangles width and length of screen-window
  for (let i = 0; i < width; i += b){
    for (var j = 0; j < height; j += b){
      trisquare(i,j);
    }
  }
}

//I created 4 triangles into a square
function trisquare(i,j) {
  noStroke();

  //the variable colorChoice is what chooses a random number from the array of reds, greens and blues
  //floor is used, so it chooses an even number
  colorChoice = floor(random(4));
  fill(red[colorChoice],green[colorChoice],blue[colorChoice]);

  //the triangle is made up of variables
  triangle(c+i,c+j,c+i,b+j,a+i,a+j);
  //checking if it is working..
  console.log(colorChoice)

  colorChoice = floor(random(4));
  fill(red[colorChoice],green[colorChoice],blue[colorChoice]);
  triangle(c+i,c+j,b+i,c+j,a+i,a+j);
  colorChoice = floor(random(4));
  fill(red[colorChoice],green[colorChoice],blue[colorChoice]);
  triangle(b+i,c+j,b+i,b+j,a+i,a+j);
  colorChoice = floor(random(4));
  fill(red[colorChoice],green[colorChoice],blue[colorChoice]);
  triangle(c+i,b+j,b+i,b+j,a+i,a+j);
}
