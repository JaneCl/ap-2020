//MiniEx4 - Capture All
// The blue square with all the buttons symbolizes a phone or general screen - so what happens
//around it is the 'data-capturing' you do not normally see
//and to make it a little fun for this excercie - you are making a
//masterpiece instead of logging data!
// Made by Jane Clausen

var input;               //Input box
var button1;             //Submit button
var greeting;            //Greeting for input box
var button2;             //Like me button
var theLikeMeButton = 0; //function for like me button
var button3;             //NEXT button
var theNextButton = 0;   //function for next button
var theDropButton = 0;   //function for drop down button
var button4;             //BACK button
var clear = 0;           //funtion for back button

function setup() {
  createCanvas(windowWidth, windowHeight);

//input bar - write what you want
  input = createInput();
  input.position(width/2-110,220);

//'submit' button - button1
  button = createButton('submit');
  button.position(input.x + input.width, 220);
  button.mousePressed(secret);

//the greeting before input bar
  greeting = createElement('h5', 'tell a secret :)');
  greeting.position(width/2-108, 180);

  textAlign(CENTER);      //positioning of the appering text
  textSize(50);           //size of appearing text

//button 2 - LIKE ME
  button2 = createButton("LIKE ME");
  button2.position(width/2-60,320);
  button2.mousePressed(exclamationPoint);

//button 3 - NEXT
  button3 = createButton("NEXT");
  button3.position(width/2+50,535);
  button3.mousePressed(circles);

//Choose a word:
  sel = createSelect();
  sel.position(width/2+15, 442);
  sel.option('koala bear');
  sel.option('hedgehog');
  sel.option('kitten');
  sel.changed(mySelectEvent);

//button4 - clear button
  button4 = createButton("BACK");
  button4.position(width/2-100,535);
  button4.mousePressed(clear);

//trying to make radio buttons
/*
  choose = createRadio();
  choose.option('Bubbles');
  choose.option('Rain');
  choose.option('Stars');
  choose.style('width','80px');
  choose.position(width/2-100,400);

  go = createButton('GO');
  go.position(width/2,420);
  go.mousePressed(drawShape);
*/

}

function draw() {
//the blue square - what you interact with
  stroke(168, 226, 255);
  strokeWeight(3);
  noFill();
  rectMode(CENTER);
  rect(width/2-1,height/2-1,270,420,10);
  noStroke();
  fill(168, 226, 255);
  rect(width/2,height/2,250,400,5);

//the PLEEEASE
  push();
  textSize(14);
  fill(0);
  text('PLEEEASE!', width/2+50,335);
  pop();


//button2 - LIKE ME is being clicked:
  if (theLikeMeButton == 1) {
    push();
    frameRate(3);
    fill(255,250,random(200));
    quad(width/10,20,width/5,20,width/6,400,width/8,400);
    rectMode(CENTER);
    rect(width/7+5,450,80,80,40);
    pop();
  }

//button 3 - NEXT - if clicked
  if (theNextButton == 1) {
    fill(255,250,200);
    ellipse(width/1, height/1,800,800);

  }

//text for drop down
  push();
  textSize(14);
  fill(0);
  text('Choose an animal', width/2-50, 455);
  pop();

//BACK button/button4
  if (clear == 1) {
    clear();
  }

}

//the secret you write - function works with mousePressed on submit button
function secret() {
  const name = input.value();

//for loop is what places your secret all over the canvas
  for (let i = 0; i < 200; i++) {
    push();
    fill(random(255), 200, 255,100);
    translate(random(width), random(height));
    rotate(random(2 * PI));
    text(name, 0, 0);
    pop();
  }
}

//function for button2 - LIKE ME
function exclamationPoint() {
  theLikeMeButton = 1;
}

//function for button3 - "next"
function circles() {
  theNextButton = 1;
}

//function for select button
function mySelectEvent() {
  push();
  theDropButton = 1;
  let item = sel.value();
    fill(0);
    textAlign(LEFT);
  if(item == 'koala bear') {
    fill(25, 111, 191);
    text('Koalas hug trees to keep cool' , width/2-400, 100);
  }
  else if(item == 'hedgehog') {
    fill(250, 165, 206);
    text('a group of hedgehogs is called an array!', width/8, 700);
  }
  else if(item == 'kitten') {
    textSize(30);
    text('kittens can be right or left-pawed', width/2+200, 200);
  }
  pop();
}



//trying to make radio buttons
/*
function drawShape() {
  fill(160,200,random(170));
  xloc = 200+random(width);
  yloc = 200+random(height);

  if (choose.value() == 'Bubbles') {
    ellipse(xloc,yloc,50,50);
  }
  if (choose.value() == 'Rain') {
    raindrop(x,y,50);
  }
}
*/
