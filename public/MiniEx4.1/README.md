Link to MiniEx4 - [Secrets](https://janecl.gitlab.io/ap-2020/MiniEx4.1/)

Link to repository: https://gitlab.com/JaneCl/ap-2020/-/tree/master/public/MiniEx4.1

<h3> Title: Secrets </h3>

__Short description:__ My interpretation and idea for a piece inspired by the headline ‘Capture All’ is all about transparency. Make the outputs to our inputs visible. 
Show how every click, every search, everything you do on the internet is being captured and saved. The thought is that the data you put in appears on your own screen - 
you get to see the output, the data captured. Although here, the output comes in shapes, forms and text, and every click changes the canvas in some shape or form.  

Everything you do online leaves a trace - leaves something behind *to* be traced, *to* be captured - this piece is thought of as making that trace transparent - it is meant 
for entertainment but also a way to become aware of your actions as they will be captured and used for statistics, advertisements, and so much more.  

![MiniEx4 - secrets](/public/MiniEx4.1/screenshot.miniex4.png "miniex4")


__Describe your program:__ 
The idea was, that I wanted to make a code where the user could personalize how they wanted the canvas to look. So that if they had clicked a button and seen the shape 
there and didn’t want it, they could click again, and it would disappear. I got halfway there. I made the shapes and text appear when clicking the buttons and I found 
that quite difficult in itself so making them disappear again turned out to be a lost cause for now. 
To make the code I’ve used a few different DOM elements such as createButton, createInput and createSelect, and used .position and .mousePressed to place the elements and 
make shapes appear when they where pressed. At first, I had almost nothing in draw, and all of it in setup or in their own functions. This made it hard to change around in
the code so I put a lot of it down in function draw(); by creating variables and making if-statements to them. For this MiniEx I’ve learned a lot by looking at examples and 
modifying codes. For example with the input bar I used the [p5.js example](https://p5js.org/examples/dom-input-and-button.html) and 
for [the drop down select I used a p5 sketch](https://editor.p5js.org/aferriss/sketches/SJtxrLp3M) as well. The name of my code is MiniEx4.1 as I 
ended up starting over and one by one copy pasting my codes into a new sketch because I could not despite trying really hard not make my code work again. 
So by slowly getting each button to work again step by step felt great and I think it was a good choice to start over as I do not think I could have found 
the error that made the old one crash! I wanted to have more buttons, sliders, and radio buttons to make the piece even more engaging and for the user to personalize 
it more, but time and abilities caught up with me. 

__Articulate how your program and thinking address the theme ‘capture all’:__ 
When I read the transmediale call the immediate thought was transparency. It feels like all the data every click, every search, every video seen, is being watched, stored 
and used for and against us. I'm never really sure when I'm being watched, all I do is say OK to a cookie. 
I realise, that these shapes and forms I created as ‘captured data’ are not harmful or being used by a third party or for own statistics, they do not save your birthday or 
turn on your microphone to listen in on conversation. It does not track what you like so they can later try and sell that to you. Instead it creates a piece - and the thought
was that the more you click the more personalized it gets. Just like the more data we produce the more they know about us. 

The subject of data and how it is being distributed on the web is discussed in Gerlitz and Helmond “The Like Economy: Social Buttons and the Data-Intensive Web” where 
they wright about Facebook and their social buttons and creating a like economy that enhances their data capturing. Data capture is when an object is identified and the 
data from it gets collected and distributed in computer systems. Facebook does this with their so-called social buttons – the like and share buttons used on many other 
websites outside of facebooks’ own. This gets described as "’making it so all websites can work together to build a more comprehensive map of connections and create better,
more social experiences for everyone’ (Zuckerberg, 2010)." p. 1349. In my understanding this is just too perfect, this might be Zuckerberg’s intention, but it is certainly 
not the only gain he gets out of this. All the data Facebook collects gets instantly turned into valuable consumer data and exchanged for money (look p. 1349). Many, if not 
all the advertisements we see on social media is chosen for the individual based on our data. 
Another issue with quantified data, like what Facebook collects from our use of the internet, is that it is amenable to statistical manipulation. As mentioned in the 
end of the Gerlitz and Helmond text, the like economy creates a slightly morphed insight of realities. Because it enables only particular forms of social engagements,
only the social web experience, one where social interaction gets metrified and multiplied and uses it together with your user profile and the social graph. It all gets 
filtered for positive and scalable affects which creates this very one-sided version of the web and social interactions on them. 
Since this article was published, we’ve gotten the reaction buttons, so maybe now the like economy might look a bit different, but I think the point coming across 
in the text is quite eye opening and makes you think about your use online and what you’re saying ‘yes’ to every time you accept a cookie or T&C’s or even just is 
a user on Facebook. Every move you take on the web is being tracked by somebody/thing whether it’s Facebook, an algorithm, analytics, google etc…  
