var soul;
var identity;

var me;
var you;
var isIt;
var thatIs;
var preload = downloadingMe;
var draw = creatingANewMe;
var setup = preparing;

function downloadingMe() {

  soul = loadJSON("animals.json")
  identity = loadJSON("words.json")
  prettyFace = loadFont("Looks/whatDoYouSee.ttf")
  appearance = loadFont("Looks/doesItMatter.otf")
}

function preparing() {
  createCanvas(windowWidth,windowHeight);
  frameRate(40);
  me = soul.animals;
  you = identity.words;
  isIt = random(me);
  thatIs = random(you);
}

function creatingANewMe() {
  background(230,230,250);

  //who is it? Animals it is:
  textFont(prettyFace);
  textAlign(LEFT);
  textSize(70);
  fill(128,0,128);
  text(isIt, width/2-100, height/2);

  //is it you?
  textFont(appearance);
  textAlign(RIGHT);
  textSize(70);
  fill(47,206,233);
  text(thatIs, width/2-130, height/2);

  //time passing - changing you.it.me.they.
  if (frameCount % 10 == 0) {
    isIt = random(me);
    thatIs = random(you);
  }
}
