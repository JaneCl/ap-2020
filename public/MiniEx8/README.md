README and RUNME made by Julie Steffensen and Jane Clausen


Link to MiniEx8 - [E-lit](https://janecl.gitlab.io/ap-2020/MiniEx8/)

Link to [repository](https://gitlab.com/JaneCl/ap-2020/-/tree/master/public/MiniEx8)


## Title: Random Function

**Short Describtion:** 

Our initial inspiration for this work was the poem creator ‘a house of dust’ https://nickm.com/memslam/a_house_of_dust.html where each 
poem starts off in the same way, but the end of each sentence is something new.
The work at its façade does not make much sense – most of the time it is not even grammatically correct. Oftentimes it is missing an article making the 
sentences seem very childlike. The intent with this was to make something that did not take itself too seriously at first glance. Trying to carry the sentiment 
of not making much sense over to the code itself gives the sketch and code a cohesive connection. This was done in an effort to try and play with the possibilities 
of changing the look and meaning of the code itself. 
The code plays with the words within the words.json-file which can be seen as an uncertainty of who you are or who what is?

  me = soul.animals;
  you = identity.words;
  isIt = random(me);
  thatIs = random(you);
  
There is no true takeaway from this e-lit poem, as we are all different, takes different paths in life and whatever identity we take on is ours. It is a queer code 
in the sense that it does not make much sense and there is no one right answer. 

![MiniEx8](/public/MiniEx8/screenshot.png "miniex8")

**Describe Program:**

at its core our program is honestly pretty simple with text changing once in a while. Making it change seperately turned out to be the most time 
consuming part of the code for us this week as we started out making it a much more complicated affair than it ever needed to be. This aside our code is connected 
to two .json files accessed via the preload() function. As we wanted the text to change continuously we chose to make several variables – first one leading us into
the right place of the json.file: me = soul.animals; and then one to make the choice of animal random: isIt = random(me);  so that when using ‘isIt’ in the function 
text() it would automatically change between the animals of the .json file. 
To slow down the framecount we’ve used an if statement with a modulo operator. This, we learned, can be used together with frameCount:  if (frameCount % 10 == 0) – this
means a new animal appears every 10 frames. Lastly, we added fonts which none of us had done before. This is also added in preload() but with the function loadFont() and 
it worked pretty seamlessly.

**Reflection upon the subject Vocable Code**
 
For this week our assignment was to create vocable code. For this week we wanted to focus on identity. Which is why we created  JSON files that show how 
the soul is the animals and the words are the identity. Which are split up by me and you. This speaks to the changing souls and identities we go through all 
our lives and how this can bring us closer to some and give us a sense of belonging and in other cases it can make us feel alien and further apart from the people 
around us. We chose sentence of identity and belonging as the first part our generative art and put this together with different animals to illustrate the ever-changing 
roles and boxes, we are put in.  Animals was a great way to illustrate this in an over characterized way.
 
Our font variables clearly relate to outside perception of beauty. We have named our font variables “prettyface” and “appearance” since the text would function without 
these appearance related Syntaxes.  The names of these variables actual attest to the function of the font that in a way dress up the text in a “prettyface” and update 
the “appearance” of it. This relates to the text the aesthetics of code. In the way that we aimed to make a code that went beyond its written form and instead created a 
deeper meaning through its belonging with the execution.
 
_“Like poetry, the aesthetic value of code lies in its execution, not simply its written form.”_ – the aesthetics of generative code.
We chose to make the statements random to create ever changing identities and social situation. In this way the statement changes every 20 seconds or so and becomes 
something new with a different focus. An example could be “I am Zebra” and then it would change to “You are eagle”. The first statement focusses on the identity of 
oneself and the next statement focus to the identity of other maybe even in relation to what oneself is. In relation to the text our generative code keeps changing 
focus by randomly changing the statements even though the code stays the same.
_“An obvious parallel to poetry can be made in that word order can help to express what is most important in a particular statement - the condition or the action.”_ – 
The aesthetics of code
Our code gives away clues on how we want the generative part to be perceived. Our code speaks a message through its entirety which is well described in this quote.
_“Code is only really understandable with the context of its overall structure – this is what makes it like a language “_ – the aesthetics of generative code
