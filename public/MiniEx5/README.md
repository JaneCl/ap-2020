Link to MiniEx4 - [Revisit the Past](https://janecl.gitlab.io/ap-2020/MiniEx5/)

Link to repository: https://gitlab.com/JaneCl/ap-2020/-/tree/master/public/MiniEx5


<h2> Mini Exercise 5: Revisit the past </h2>

When getting the assignment, I wanted to give my very first program an upgrade, by making it interactive in some way and use syntaxes learnt in the time since 
making it the first time. I wanted to see if I could better the code in some way as well, by making it more precise compared to last time. 
Based on the feedback I got the first time around I wanted to take inspiration from our week with throbbers as the original sketch very much resembled one. 

Before it was a more static piece, where the only movement was the circles getting bigger and smaller – now they also circle around an axis and opacity and colour 
changes depending on mouseX and -Y. 
I made them rotate around an axis by using translate to change the placement (0,0) and then I had to change the coordinate on every ellipse as well to fit around 
the new axis. Originally, I thought I would make a function for the ellipses to make it more effective. After working with it for a bit I realized I would end up 
with just as many lines of code as they all have different colours and also a few different ways of moving (bigger and smaller in different tempos). Which is why 
you will not find a self-made function in the code. 
Instead I created the following variables:


  let r = (mouseX-350+200) * (255/700);
  
  let a = mouseY * (255/500);


-350 was added to mouseX because I did not want the ellipsis to become black and then adding 200 back because they turned white too quickly. 
Adding r and a to my many fill(); created a feeling of interactivity to the sketch. 
Lastly, I added a for loop to create a subtle play in the background. 

![MiniEx5](/public/MiniEx5/screenshot5.png "miniex5")


<h4> Aesthetic Programming: </h4>

Aesthetic programming is a way to express yourself through code. It is about thinking and acting critically through and with code. When trying 
to explain Aesthetic Programming Soon & Cox (2020) write: _“we consider programming to be a dynamic cultural practice and phenomenon, a way of 
thinking and doing in the world, and a means to understand some of the complex procedures that underwrite our lived realities, in order to act upon 
those realities"_ code or programming is not just used to create a website or an app, but code is a part of the cultural production and as such they 
should be seen in a social and political context. When there is being talked about aesthetics it is not necessarily about whether or not a program is 
pretty or beautiful but also what the programs portrays – the message it sends through its actions, appearance and behaviour. This, I think, could also 
be called exploratory programming as Nick Montfort calls it. He describes it as using computation as a way to inquire and constructively think about important issues. 
After having programmed for around 5 weeks now, I am beginning to see how this could be possible, see how programming could make a statement or explore 
a topic in a different way than traditional artforms could. Thinking back on Transmediale I see how the combination of programming and something tangible 
created these thought-provoking installations. 
For now, at my current programming level, I think aesthetic programming for me is more about the way in which coding a program can make you feel like you 
are contributing something to the world – even if it is just small and insignificant – just like if you’d made a cup out of clay, written a novel or painted 
a picture. It allows me to think in a completely new way and an opportunity to understand digital culture. 

When trying to answer the question of what the relation between programming and digital culture is, I think of open source. Of how you as a programmer 
and a user can contribute to making the software even better or change the meaning of it just by the way you are understanding the code compared to somebody 
else. You as a programmer can influence the digital culture – off course only the parts that are open source and available for you. Digital culture is the 
aspects of technology that is social, cultural, ethical and aesthetic – it is everything we meet in the digital world. The problem with this is algorithms, 
the googles and facebooks of the world, that rules and regulates what the consumer sees. We have read a text in Software Studies about the metainterface 
which is quite fitting in this context as well. It talks about the curation of the internet and how Software Art is difficult to find on the internet as 
it does not appear in e.g. app stores. 
The ‘direct’ relation between programming and digital culture is that programming by extension is inseparable from digital culture as programming is the 
foundation on which digital culture is built on. 

