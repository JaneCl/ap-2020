//MiniEx5 - Pause Week
//This program is based on MiniEx1
//Made by Jane Clausen

//variables
let diameter;
let angle = 0;
let centerX = 350;
let centerY = 250;


function setup() {
  createCanvas(700,500);
  //diameter of the ellipsis
  diameter = 70;
}


function draw() {
  background(255, 204, 100);


  //background lines - to show I know how to make a for loop
  for (let y=0; y <= width; y = y + 10) {
    stroke(235, 185, 84);
    line(y,0,y,y);
  }


  //changed 0,0 coordinate to make the circles move around the center
  translate(centerX,centerY);
  rotate(angle);


  //mouseX defines the colour of the circles
  //the mouseY defines the opacity of the circles
  let r = (mouseX-350+200) * (255/700);
  let a = mouseY * (255/500);

  //I put a console.log to check if everything was working correctly
  console.log("testing")



  //the formulas to make the circles bigger and smaller
  let d1 = 10 + (sin(angle) * diameter) / 2 + diameter;
  let d2 = 10 + (sin(angle + PI / 2) * diameter) / 2 + diameter / 2;
  let d3 = 10 + (sin(angle + PI) * diameter) / 2 + diameter / 2;
  let d4 = 10 + (sin(angle + PI / 4) * diameter) / 2 + diameter /2;



  //The ellipsis
  //the r's and a's are the variables added to make them interact with mouseX and -Y.
  noStroke();
  fill(127+r,0+r,0+r,a);
  ellipse(0,-165,d1,d1);
  fill(242+r, 58+r, 113+r,a);
  ellipse(165,0,d2,d2);
  fill(255+r,0+r,0+r,a+20);
  ellipse(0,165,d3,d3);
  fill(255+r,200+r,200+r,a);
  ellipse(-165,0,d4,d4);
  fill(255+r,200+r,200+r,a+40);
  ellipse(70,-130,d4,d4);
  fill(255+r,150+r,190+r,a);
  ellipse(130,-70,d3,d3);
  fill(255+r,100+r,140+r,a+15);
  ellipse(130,70,d2,d2);
  fill(255+r,50+r,100+r,a);
  ellipse(70,130,d1,d1);
  fill(255+r,120+r,120+r,a);
  ellipse(-70,130,d4,d4);
  fill(127+r,0+r,10+r,a+20);
  ellipse(-130,70,d1,d1);
  fill(255+r,160+r,100+r,a);
  ellipse(-130,-70,d2,d2);
  fill(255+r,150+r,140+r,a);
  ellipse(-70,-130,d3,d3);



  //angle is the speed of the movement
  angle += 0.01;
}
