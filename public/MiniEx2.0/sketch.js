//sweating globe emoji
//includes image of earth
let img;
function preload() {
  img = loadImage('globe.png');
}

function setup() {
  createCanvas(800,600);
  background(173, 224, 247);
  //image of the globe
  image(img,275,150,250,250);

  //the smiley
  //the circle
  fill(247, 129, 32,100);
  stroke(194, 82, 21);
  strokeWeight(5);
  ellipse(400,275,250,250);

  //cheeks
  fill(214, 90, 24,80);
  noStroke();
  ellipse(330,310,40,20);
  ellipse(470,310,40,20);
}

function draw() {

  //the eyes
  fill(84, 29, 0);
  stroke(171, 67, 12);
  strokeWeight(3);
  ellipse(360,270,30,40);
  ellipse(440,270,30,40);

  //mouth
  fill(54, 18, 0);
  stroke(171, 67, 12);
  ellipse(400,350,100,40);

  //tongue
  fill(245, 64, 121);
  stroke(186, 26, 76);
  strokeWeight(3);
  beginShape();
    curveVertex(375, 350);
    curveVertex(375, 350);
    curveVertex(380, 410);
    curveVertex(420, 410);
    curveVertex(425, 350);
    curveVertex(425, 350);
  endShape();
  stroke(181, 36, 81);
  line(400,358,400,390);

  //left eyebrow
  noFill();
  stroke(112, 70, 47);
  strokeWeight(5);
  beginShape();
    curveVertex(310, 240);
    curveVertex(310, 240);
    curveVertex(325, 237);
    curveVertex(340, 231);
    curveVertex(350, 225);
    curveVertex(350, 225);
    curveVertex(360, 218);
    curveVertex(360, 218);
  endShape();

  //right eyebrow (mirrored the left one)
  beginShape()
    curveVertex(490, 240);
    curveVertex(490, 240);
    curveVertex(475, 237);
    curveVertex(460, 231);
    curveVertex(450, 225);
    curveVertex(450, 225);
    curveVertex(440, 218);
    curveVertex(440, 218);
  endShape();

  //drop of sweat
  fill(51, 190, 255);
  stroke(7, 150, 217);
  beginShape();
    vertex(260,319);
    vertex(290,270);
    vertex(290,270);
    vertex(320,319);
  endShape();
    arc(290, 325, 60, 60, -0.3, PI-6,OPEN);



}
